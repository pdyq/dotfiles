set tabstop=4 softtabstop=4 shiftwidth=4 expandtab smartindent
set nohlsearch hidden
set number relativenumber
set ignorecase noswapfile nobackup
set incsearch wildmenu
set scrolloff=8
set encoding=utf8 ffs=unix,dos,mac
set cursorline cursorcolumn
map J gT
map K gt
packadd! dracula
syntax enable
colorscheme dracula

