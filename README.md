# pdyq's dotfiles

This stores dotfiles of pdyq from different softwares. 
Most of them are garbage btw and it doesn't worth your time.

# Software
[alacritty](https://github.com/alacritty/alacritty) - a terminal emulator  
[dunst](https://github.com/dunst-project/dunst) - a notification daemon  
[fish](https://github.com/fish-shell/fish-shell) - a shell  
[Hyprland](https://github.com/hyprwm/Hyprland) - a Wayland compositor  
[hyprpaper](https://github.com/hyprwm/hyprpaper) - a wallpaper utility for Hyprland  
[i3-gaps](https://github.com/irshadcc/i3-gaps) - a Xorg window manager  
> [i3-gaps](https://github.com/irshadcc/i3-gaps) will be merged with [i3](https://github.com/i3/i3.github.io) in the future.  

[i3-master-layout](https://github.com/Hippo0o/i3-master-layout) - a script for i3 to use the master layout (like dwm)  
[kitty](https://github.com/kovidgoyal/kitty) - a terminal emulator  
[polybar](https://github.com/polybar/polybar) - a tool for creating status bar(s)  
[Waybar](https://github.com/Alexays/Waybar) - a wlroots-based compositors  
[starship](https://github.com/starship/starship) - a cross-shell prompt  
[vim](https://github.com/vim/vim) - **the** text editor  
[jonaburg/picom](https://github.com/jonaburg/picom/) - a fork of picom, which is a X compositor

