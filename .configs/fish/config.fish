if status is-interactive
    # Commands to run in interactive sessions can go here
end
if status is-login
    if test -z "$DISPLAY" -a "$XDG_VTNR" = 1
        exec startx -- -keeptty
    end
end

function fish_greeting
    echo 
    pfetch
end
function fish_prompt
    echo
    echo $(set_color blue) $(prompt_pwd) $(set_color green)">  "
end

cd ~
alias p="paru"
alias ls="exa -a --icons"
alias ll="exa -lah --icons"
alias src="source ~/.config/fish/config.fish"
alias v="vim"
alias icat="kitty +kitten icat --align=left"
alias j="autojump"
alias rm="trash"
alias rl="trash-list"
alias rr="trash-restore"
alias soy="doas systemctl"
set -x PF_INFO "ascii title os pkgs memory"
[ -f /usr/share/autojump/autojump.fish ]; and source /usr/share/autojump/autojump.fish
# cat ~/.cache/wal/sequences
